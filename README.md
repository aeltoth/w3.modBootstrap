# Witcher 3 Bootstrap Mod

Utility mod for "The Witcher 3: Wild Hunt" game by CD Projekt Red which provides a standardized autostartup capability for other mods, support for custom area hubs handling, some utilities and a reusable list menu.

For more information see the [modBootstrap page][nexusmods-bootstrap] on nexusmods.


## Building project

1. Copy `_settings_.bat-template` to `_settings_.bat`

2. Adjust the paths `DIR_W3`, `DIR_MODKIT` and optionally `DIR_ENCODER` in `_settings_.bat` according to your local setup.

3. Build with:
   ```sh
   > full.rebuild.bat
   ```

This will regenerate the full bootstrap mod and deploy it into the configured Witcher 3 directory.

In addition you have to copy `modBootstrap-registry` and `modBootstrap-vanilla-patch` into the mods folder of the W3 installation.

## Contributing

If you have questions, you can find me on the [nexusmods discord server][nexusmods-discord] in the #the-witcher-3 channel.

[nexusmods-discord]:       https://discord.gg/tJDuqw5
[nexusmods-bootstrap]:     https://www.nexusmods.com/witcher3/mods/2109
